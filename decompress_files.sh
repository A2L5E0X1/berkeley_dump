#!/bin/bash

FILES="system/system/apex/com.android.runtime.release.apex.gz system/system/priv-app/Settings/Settings.apk.gz"

if [[ "$(which gunzip)" = "" ]]; then
    echo "Missing dependencies: gunzip"
    exit 1
fi

for f in "$FILES"; do gunzip $f; done
